#include "Lock.h"
Lock::Lock() { };
Lock::~Lock() { };
const DigCom *Lock::getCN() { return &cn; };
const DigCom *Lock::getLN() { return &ln; };
const DigCom *Lock::getHN() { return &hn; };
void Lock::setCN(const DigCom &digcom) { cn = digcom; };
void Lock::setLN(const DigCom &digcom) { ln = digcom; };
void Lock::setHN(const DigCom &digcom) { hn = digcom; };
void Lock::setCN(int x) {
	cn.set4digits(x);
	return;
};

bool Lock::isValidCNLock() {
     return cn.isValidCN();
};

std::ostream& operator<<(std::ostream& ostr, Lock lock) {
	ostr << "CN: " << *lock.getCN() << "\n";
	ostr << "LN: " << *lock.getLN() << "\n";
	ostr << "HN: " << *lock.getHN() << "\n";
	return ostr;
};