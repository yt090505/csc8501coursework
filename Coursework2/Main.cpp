#include <iostream>
#include <fstream>
#include <array>
#include <ctime>
#include <cstdlib> 
#include <vector>
#include <string>
#include <math.h>
#include <algorithm>
#include <thread> 

#include "Com.h"
#include "DigCom.h"
#include "MovCom.h"
#include "Lock.h"
#include "MultiLock.h"
#include "KeyOfLocks.h"

//////////initialize varibles/////////////
//key has stable UHF LHF PHF for "all multi-lock safes"(all SetOfLocks) in each run
KeyOfLocks kol;
std::fstream ks, ts, ls, as;
const char *keyfile, *safefile, *lockfile, *decodefile, *unlockfile;
int numberRun;
std::vector<int> uhf_vector;
std::vector<std::array<int, 3>> lns;
char yes1,yes2;
int amountLocks;
std::thread read;
std::thread decode;
std::array<int, 3> firstLock;

void initializaion() {
	//generate a random seed based on time
	srand(time(NULL));
	keyfile = "key.txt";
	safefile = "mulit-lock.txt";
	lockfile = "lock.txt";
	decodefile = "decode.txt";
	unlockfile = "unlock.txt";
	kol.randomAll();
	numberRun = 1000;
	int amountLocks = 5;
	return;
};

//coursework1
void generateKeyFile() {
	ks.open(keyfile, std::ios_base::out);
	for (int i = 0; i < numberRun; i++)
	{
		kol.randomRoot();
		ks << kol;
	};
	ks.close();
	return;
};

void readAKey() {
	DigCom new_digcom;
	MovCom new_movcom;
	int x;
	ks.ignore(5);
	ks >> x;
	new_digcom.set4digits(x);
	kol.setRoot(new_digcom);
	ks.ignore(5);
	for (int i = 0; i < 4; i++)
	{
		ks >> x;
		ks.ignore(1);
		new_movcom.setdigit(i, x);
	}
	kol.setUHF(new_movcom);
	ks.ignore(4);
	for (int i = 0; i < 4; i++)
	{
		ks >> x;
		ks.ignore(1);
		new_movcom.setdigit(i, x);
	}
	kol.setLHF(new_movcom);
	ks.ignore(4);
	for (int i = 0; i < 4; i++)
	{
		ks >> x;
		ks.ignore(1);
		new_movcom.setdigit(i, x);
	}
	kol.setPHF(new_movcom);
	return;
};

//coursework2

//output only valid safes in this case
void generateSafeANDLock() { 
	ks.open(keyfile, std::ios_base::in);
	ts.open(safefile, std::ios_base::out);
	ls.open(lockfile, std::ios_base::out);
	MultiLockVector mtl(amountLocks);
	mtl.countValid = 0;
	mtl.count = 0;

	for (int i = 0; i < numberRun; i++)
		
	{
		readAKey();
		mtl.hashAll(kol);
		mtl.count++;
		if (mtl.isValid()) {
			ls << "ROOT : " << *kol.getRoot()<<"\n";
			for (int i = 0; i < amountLocks; i++)
			{
				ls << "LN"<<i<<" : " << *mtl.locks.at(i).getLN()<< "\n";
			};
			ts << mtl;
			mtl.countValid++;
		};
	};

	ts << "COUNT OF VALID : "<<mtl.countValid;

	ks.close();
	ts.close();
	ls.close();
	return;
};

//these 2 functions read lock files, 
//transform each lock into a arr<int,3> 
//including int form of 4 positive digits root,ln0,ln1

//read first lock into a solo array
void readFirstLock() {
	int root_int = 0, ln0_int = 0, ln1_int = 0;
	ls.open(lockfile, std::ios_base::in);
	ls.ignore(7);
	ls >> root_int;
	ls.ignore(7);
	ls >> ln0_int;
	ls.ignore(7);
	ls >> ln1_int;
	firstLock = { root_int,ln0_int,ln1_int };
	ls.close();
	return;
}
//read all locks into a vector 
void readAllLocks() {
	int root_int = 0, ln0_int = 0, ln1_int = 0;
	ls.open(lockfile, std::ios_base::in);
	ls.ignore(3);
	while (!ls.eof())
	{
		ls.ignore(4);
		ls >> root_int;
		ls.ignore(7);
		ls >> ln0_int;
		ls.ignore(7);
		ls >> ln1_int;
		std::array<int, 3> new_array;
		new_array = { root_int,ln0_int,ln1_int };
		lns.push_back(new_array);
		ls.ignore(11 * amountLocks - 18);
	};
	ls.close();
	return;
};

//these 2 functions read lock files, 
//transform each lock into a arr<int,3> 
//including int form of 4 positive digits root,ln0,ln1

//read first lock in a solo array
//save all out in a vector of uhf(int form)
void decodeFirstLock() {
	KeyOfLocks _kol;
	MultiLockVector _mtl(amountLocks);
	_mtl.count = 0;
	DigCom _root, _ln0, _ln1, _hn0;
	DigCom _cn0;
	MovCom _ulphf, _uhf;

	_root.set4digits(firstLock.at(0));
	_ln0.set4digits(firstLock.at(1));
	_ln1.set4digits(firstLock.at(2));
	_ulphf = _ln1 - _ln0;
	_hn0 = _root + _ulphf;
	for (int i = 0; i < 10000; i++)
	{
		_cn0.set4digits(i);
		_uhf = _cn0 - _root;
		_kol.setRoot(_root);
		_kol.setUHF(_uhf);
		_kol.setLHF(_ln0 - _cn0);
		_kol.setPHF(_hn0 - _ln0);
		_mtl.hashAll(_kol);
		if (_mtl.isValid()) { 
			_uhf = _cn0 - _root;
			_kol.setRoot(_root);
			_kol.setUHF(_uhf);
			_kol.setLHF(_ln0 - _cn0);
			_kol.setPHF(_hn0 - _ln0);
			_mtl.hashAll(_kol);
			_mtl.count++;
			uhf_vector.push_back(_uhf.getint());
//			std::cout << _mtl<<"\n";  debug mode
		};
	};
	return;
};

//recursing Lock Vector and UHF Vector
//for each lock
//reduce rang of hash functions in number of UHF
//
// I USE TEMPLATE HERE! 
//
void decodeAllLock() {
	as.open(decodefile, std::ios_base::out);
	DigCom _root, _ln0, _ln1, _cn0, _hn0;
	MovCom _uhf, _lhf, _phf, _ulphf;
	
// a template
	MultiLock<std::vector<Lock>> mtl(amountLocks);

//optimize algorithm
//	int loop = std::min(int(lns.size()),int(pow(lns.size(),0.5)) + 10);	  
//	int loop = std::max(int(lns.size()),800);
	int loop = lns.size();
	std::array<int, 3> arr;
	for (int i = 0; i < loop; i++)
	{
		mtl.countValid = 0;
		arr = lns.at(i);
		_root.set4digits(arr.at(0));
		_ln0.set4digits(arr.at(1));
		_ln1.set4digits(arr.at(2));
		for (int j = 0; j < uhf_vector.size();j++) {

			_uhf.set4digits(uhf_vector.at(j));
			_cn0 = _root + _uhf;

			mtl.locks.at(0).setCN(_cn0);
			for (int k = 1; k < amountLocks; k++)
			{
				mtl.locks.at(k).setCN(*mtl.locks.at(k - 1).getCN() + _ulphf);
			};
			if (mtl.isValid()) {
				mtl.countValid++;
			}
			else {
				uhf_vector.erase(uhf_vector.begin() + j);
				j--;
			};
		};
		as << "recursing Lock " << i + 1  << ", Number of Valid UHF :" << mtl.countValid << std::endl;
	}
	//output result to a file
	for (int i = 0; i < uhf_vector.size(); i++)
	{
		arr = lns.at(0);
		_root.set4digits(arr.at(0));
		_uhf.set4digits(uhf_vector.at(i));
		_cn0 = _root + _uhf;
		_ln0.set4digits(arr.at(1));
		_ln1.set4digits(arr.at(2));
		_ulphf = _ln1 - _ln0;
		_lhf = _ln0 - _cn0;
		_hn0 = _root + _ulphf;
		_phf = _hn0 - _ln0;
		as << "KN : " << i + 1 << "\n" << _uhf << "\n" << _lhf << "\n" << _phf << "\n";
	}
	as.close();
	return;
}

//read original safe file
//to get unique answer
//try unlock mtl_1 at cn_0
void seeAnswer(){
	DigCom _root, _ln0, _ln1, _cn0, _hn0;
	MovCom _uhf, _lhf, _phf, _ulphf;
	int _cn0_int = 0;
	DigCom _cn0_original;

	as.open(unlockfile, std::ios_base::out);
	ts.open(safefile, std::ios_base::in);

	ts.ignore(19);
    ts >> _cn0_int;
	_cn0_original.set4digits(_cn0_int);
    _root.set4digits(  lns.at(0).at(0) );
	_ln0.set4digits(lns.at(0).at(1));
	_ln1.set4digits(lns.at(0).at(2));

	for (int i = 0; i < uhf_vector.size(); i++)
	{
			_uhf.set4digits(uhf_vector.at(i));
			_ulphf = _ln1 - _ln0;
			_cn0 = _root + _uhf;
			_lhf = _ln0 - _cn0;
			_hn0 = _root + _ulphf;
			_phf = _hn0 - _ln0;
			if (_cn0 == _cn0_original) {
				as << "Key : RIGHT" << std::endl;
				as << _root << std::endl << _uhf << std::endl << _lhf << std::endl << _phf << std::endl;
				break;
			};
	};
	ts.close();
	as.close();
	return;
};

////////////////////////interface
void greeting() {
	std::cout << "#####" << "#####" << "#####" << "#####" << "#####" << "#####" << "\n";
	std::cout << "Greetings" << "\n";
	std::cout << "#####" << "#####" << "#####" << "#####" << "#####" << "#####" << "\n";
	return;
};

void setNumberRun() {
	std::cout << "Please enter a number(1 to 10,000) of keys you will creat\n";
	if (!(std::cin >> numberRun)) {
		std::cout << "Set to default number 1000\n";
		std::cin.clear();
		std::string ignoreLine; //read the invalid input into it
		std::getline(std::cin, ignoreLine); //read the line till next space
	};
	if (numberRun < 1)numberRun = 1;
	else if (numberRun > 10000) numberRun = 10000;
	std::cout << "Crafting " << numberRun << " Keys\n";
	return;
};

void unlockPrompt() {
	std::cout << "Breaking Multi-lock Safe No.1\n";
	std::cout << "Unique key found, saved in '" << unlockfile << "'\n";
};

///////////////////////////////////////main/////////////////////////////////////////

void main() {

	initializaion();

	greeting();
	std::cout << "Set Number of Locks in each 'multi-lock safe\n";
	if (!(std::cin >> amountLocks)) { 
		std::cin.clear(); 
		std::cout << "Number of Locks set to Default(5)";
	}
	if (amountLocks < 2) {
		amountLocks = 2;
		std::cout << "Number of Locks set to Minimum(2)";
	}
	else
	{
		std::cout << "Number of Locks set to " << amountLocks<<"\n\n";
	}
	MultiLockVector mtl(amountLocks);
letsgo:
	{
		setNumberRun();
		generateKeyFile();
		generateSafeANDLock();
		std::cout << "Valid Multi-Lock : " << mtl.countValid;
		std::cout << "  ;  Total Multi-lock : " << mtl.count << "\n\n";
		//if no valid key have to regenerate
		if (mtl.countValid == 0) {
			std::cout << "\nNo lock found, must regenerate.\n";
			goto randomHF;
		}
		std::cout << "Wanna regenerate? (y/n) \n";
		if (!(std::cin >> yes1)) {
			std::cin.clear();
			std::string ignoreLine;
			std::getline(std::cin, ignoreLine);
		};
		if (yes1 != 'y') { goto bottom; }
	}
randomHF:
	{
		std::cout << "Wanna randomize hash numbers? (y/n) \n";
		if (!(std::cin >> yes2)) {
			std::cin.clear();
		}
		std::string ignoreLine;
		std::getline(std::cin, ignoreLine);
		if (yes2 == 'y') {
			kol.randomAll();
		}
		goto letsgo;
		std::cout << "\nDecoding lock file... ...\n";
	}
bottom: {}
//
//I USE THREADING HERE
//
	readFirstLock();
	std::thread read(readAllLocks);
	std::thread decode(decodeFirstLock);
	std::cout << "Threading starts...\n";
	read.join();
	decode.join();
	std::cout << "Threading ends...\n";
	std::cout << "Possible outs by decoding the first Mulit-Lock:" << uhf_vector.size() <<"\n";
//recursion using 2 result of threading 
	decodeAllLock();
	std::cout << "\n" << lns.size() << " Recursion done\n";
	std::cout << "Possible outs by decoding all Mulit-Lock:" << uhf_vector.size()<<"\nAll possible outs in 'decode.txt'"<<"\n\nWanna cheat?(y/n)\n";
	if (!(std::cin >> yes1)) std::cin.clear();
	if (yes1 == 'y') {
		seeAnswer();
		unlockPrompt();
	}
	std::cout << "\nInput any to exit.";
	std::cin >> yes1;
	return;
};