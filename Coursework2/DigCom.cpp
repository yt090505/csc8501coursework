#include "DigCom.h"

DigCom::DigCom() { digits = { 0,0,0,0 }; };

DigCom::~DigCom() {};

DigCom DigCom::operator= (const DigCom &rhs) { 
	for (int i = 0; i < 4; i++)
	{
		digits.at(i) = rhs.getdigit(i);
	};
	return *this;
};

//DigCom DigCom::operator+ (const MovCom &rhs) {
//	DigCom result = DigCom();
//	for (int i = 0; i < 4; i++)
//	{
//		result.digits.at(i) = digits.at(i)+ rhs.getdigit(i);
//	};
//	return result;
//};

void DigCom::setdigit(int x, int val) {
	digits.at(x) = (val + 50) % 10;
	return;
};

void DigCom::random() {
	int ran = rand() % 10000;
	set4digits(ran);
	return;
};

bool DigCom::isValidCN() {
	for (int i = 0; i < 4; i++)
	{
		for (int j = i + 1; j < 4; j++)
		{
			if (digits.at(i) == digits.at(j)) { return false; };
		}
	};
	return true;
};

std::ostream& operator<<(std::ostream& ostr, const DigCom &digcom) {
	for (int i = 0; i < 4; i++)
	{
		ostr << digcom.getdigit(i);
	}
	return ostr;
};

bool operator==(const DigCom &a1, const DigCom &a2) {
	for (int i = 0; i < 4; i++)
	{
		if (a1.getdigit(i) != a2.getdigit(i))
		{
			return false;
		};
	};
	return true;
};


DigCom operator+(const DigCom &lhs, const MovCom &rhs) {
	DigCom rsl;
	for (int i = 0; i < 4; i++)
	{
		rsl.setdigit(i, lhs.getdigit(i) + rhs.getdigit(i));
	};
	return rsl;
};

MovCom operator- (const DigCom &lhs, const DigCom &rhs) {
	MovCom rsl;
	for (int i = 0; i < 4; i++)
	{
		rsl.setdigit(i, lhs.getdigit(i) - rhs.getdigit(i) + 20);
	};
	return rsl;
};
