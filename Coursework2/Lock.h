#ifndef LOCK_H
#define LOCK_H
#include "DigCom.h"

//a single lock namely Lock0/1/2/3/4
class Lock {
public:
	Lock();
	~Lock();
	const DigCom *getCN() ;
	const DigCom *getLN() ;
	const DigCom *getHN() ;
	void setCN(const DigCom &digcom) ;
	void setCN(int x);
	void setLN(const DigCom &digcom) ;
	void setHN(const DigCom &digcom) ;
	bool isValidCNLock();
	friend std::ostream& operator<<(std::ostream& ostr, Lock lock);
protected:
	DigCom cn;
	DigCom ln;
	DigCom hn;
};

#endif