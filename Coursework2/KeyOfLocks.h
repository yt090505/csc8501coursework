#ifndef KEYOFLOCKS_H
#define KEYOFLOCKS_H
#include "DigCom.h"
#include "MovCom.h"

class KeyOfLocks {
public:
	KeyOfLocks();
	~KeyOfLocks();
	const MovCom * getUHF() ;
	const MovCom * getLHF() ;
	const MovCom * getPHF() ;
	const DigCom * getRoot() ;
	void setUHF(MovCom movcom) ;
	void setLHF(MovCom movcom) ;
	void setPHF(MovCom movcom) ;
	void randomAll() ;
	void randomRoot() ;
	void setRoot(DigCom digcom) ;
	friend std::ostream& operator<<(std::ostream& ostr, KeyOfLocks kol) ;
protected:
	DigCom root;
	MovCom uhf, lhf, phf;
};

#endif 

