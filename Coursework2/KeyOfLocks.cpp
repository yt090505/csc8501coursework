#include "KeyOfLocks.h"

KeyOfLocks::KeyOfLocks() {};
KeyOfLocks::~KeyOfLocks() {};
const MovCom * KeyOfLocks::getUHF() { return  &uhf; };
const MovCom * KeyOfLocks::getLHF() { return &lhf; };
const MovCom * KeyOfLocks::getPHF() { return &phf; };
const DigCom * KeyOfLocks::getRoot() { return &root; };
void KeyOfLocks::setUHF(MovCom movcom) { uhf = movcom; };
void KeyOfLocks::setLHF(MovCom movcom) { lhf = movcom; };
void KeyOfLocks::setPHF(MovCom movcom) { phf = movcom; };

void KeyOfLocks::randomAll() {
	uhf.random();
	lhf.random();
	phf.random();
	root.random();
	return;
};

void KeyOfLocks::randomRoot() { 
	root.random(); 
	return;
};

void KeyOfLocks::setRoot(DigCom digcom) { root = digcom; };

std::ostream& operator<<(std::ostream& ostr, KeyOfLocks kol) {
	ostr << "ROOT " << *kol.getRoot() << "\n";
	ostr << "UHF " << *kol.getUHF() << "\n";
	ostr << "LHF " << *kol.getLHF() << "\n";
	ostr << "PHF " << *kol.getPHF() << "\n";
	return ostr;
};

