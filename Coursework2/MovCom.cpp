#include "MovCom.h"

MovCom::MovCom() { digits = { 0,0,0,0 }; };

MovCom::~MovCom() {};

MovCom MovCom::operator= (const MovCom &c) {
	for (int i = 0; i < 4; i++)
	{
		this->digits.at(i) = c.getdigit(i);
	};
	return *this;
};

void MovCom::setdigit(int x, int val) {
	digits.at(x) = val % 10;
	return;
};

void MovCom::random() {
	int ran = rand() % 130321;
	for (int i = 0; i < 4; i++)
	{
		int unit = ran % 19;
		digits.at(i) = unit - 9;
		ran = (ran - unit) / 19;
	};
	return;
};

//transType MovCom to unsigned int
int MovCom::getint() {
	int unsigned_unit = 0, result = 0;
	for (int i = 0; i < 4; i++)
	{
		int unsigned_unit = digits.at(i);
		if (unsigned_unit < 0)unsigned_unit = -unsigned_unit;
		result = result * 10 + unsigned_unit;
	};
	return result;
};

std::ostream& operator<<(std::ostream& ostr, const MovCom &movcom) {
	ostr << std::showpos;
	for (int i = 0; i < 3; i++)
	{
		ostr << movcom.getdigit(i) << ",";
	};
	ostr << movcom.getdigit(3) << std::noshowpos;
	return ostr;
};









