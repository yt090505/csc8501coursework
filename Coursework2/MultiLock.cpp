#include "MultiLock.h"
#include "KeyOfLocks.h"
#include "Lock.h"

int MultiLock5::count = 0;
int MultiLock5::countValid = 0;

MultiLock5::MultiLock5() { count++; };

MultiLock5::~MultiLock5() {};

bool MultiLock5::isValid() {
	for (int i = 0; i < 5; i++)
	{
		if (!locks.at(i).isValidCNLock())
		{
			return false;
		}
	};
	return true;
};

void addValid(MultiLock5 multilock)
{
	if (multilock.isValid())multilock.countValid++; return;
};

void MultiLock5::hashAll(KeyOfLocks key) {
	locks.at(0).setCN(*key.getRoot() + *key.getUHF());
	locks.at(0).setLN(*locks.at(0).getCN() + *key.getLHF());
	locks.at(0).setHN(*locks.at(0).getLN() + *key.getPHF());
	for (int i = 1; i < 5; i++)
	{
		locks.at(i).setCN(*locks.at(i - 1).getHN() + *key.getUHF());
		locks.at(i).setLN(*locks.at(i).getCN() + *key.getLHF());
		locks.at(i).setHN(*locks.at(i).getLN() + *key.getPHF());
	};
	return;
};

std::ostream& operator<<(std::ostream& ostr, MultiLock5 sol) {
	if (sol.isValid()) {
		ostr << "NS" << sol.count << "  IS VALID\n" ;
	}
	else {
		ostr << "NS" << sol.count << "  NOT VALID\n" ;
	};
	for (int i = 0; i < 5; i++)
	{
		ostr << "CN" << i << ": " << *sol.locks.at(i).getCN() << ", ";
		ostr << "LN" << i << ": " << *sol.locks.at(i).getLN() << ", ";
		ostr << "HN" << i << ": " << *sol.locks.at(i).getHN() << "\n";
	};
	return ostr;
};

//vector
int MultiLockVector::count = 0;

int MultiLockVector::countValid = 0;

MultiLockVector::MultiLockVector(int x) {
	for (int i = 0; i < x; i++)
	{
		Lock _lock;
		locks.push_back(_lock);
	}
	return;
};

MultiLockVector::MultiLockVector() {}

MultiLockVector::~MultiLockVector() {
	for (int i = 0; i < locks.size(); i++)
	{
		locks.pop_back();
	}
	return;
};

bool MultiLockVector::isValid() {
	for (int i = 0; i < locks.size(); i++)
	{
		if (!locks.at(i).isValidCNLock())
		{
			return false;
		}
	};
	return true;
};

void addValid(MultiLockVector multilock)
{
	if (multilock.isValid())multilock.countValid++; return;
};

void MultiLockVector::hashAll(KeyOfLocks key) {
	locks.at(0).setCN(*key.getRoot() + *key.getUHF());
	locks.at(0).setLN(*locks.at(0).getCN() + *key.getLHF());
	locks.at(0).setHN(*locks.at(0).getLN() + *key.getPHF());
	for (int i = 1; i < locks.size(); i++)
	{
		locks.at(i).setCN(*locks.at(i - 1).getHN() + *key.getUHF());
		locks.at(i).setLN(*locks.at(i).getCN() + *key.getLHF());
		locks.at(i).setHN(*locks.at(i).getLN() + *key.getPHF());
	};
	return;
};

std::ostream& operator<<(std::ostream& ostr, MultiLockVector sol) {
	if (sol.isValid()) {
		ostr << "NS" << sol.count << "  IS VALID\n";
	}
	else {
		ostr << "NS" << sol.count << "  NOT VALID\n";
	};
	for (int i = 0; i < sol.locks.size(); i++)
	{
		ostr << "CN" << i << ": " << *sol.locks.at(i).getCN() << ", ";
		ostr << "LN" << i << ": " << *sol.locks.at(i).getLN() << ", ";
		ostr << "HN" << i << ": " << *sol.locks.at(i).getHN() << "\n";
	};
	return ostr;
};

