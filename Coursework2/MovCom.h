#ifndef MOVCOM_H
#define MOVCOM_H
#include "Com.h"

//has unique set method as well
class MovCom : public Com
{
public:
	MovCom() ;
	~MovCom() ;
	MovCom operator= (const MovCom &c);
	void setdigit(int x, int val) ;
	void random();
	int getint();
	friend std::ostream& operator<<(std::ostream& ostr, const MovCom &movcom) ;
protected:
};

//template
template <typename T>
class MovComTemplar : public Com
{
public:
	MovComTemplar() {
		for (int i = 0; i < 4; i++)
		{
			digits.at(i) = T(0);
		}
		return;
	};
    ~MovComTemplar() {};

//	Compiler Error C3240

	MovComTemplar<T> MovComTemplar<T>::operator= (const MovComTemplar<T> &c) {
		for (int i = 0; i < 4; i++)
		{
			this->digits.at(i) = c.getdigit(i);
		};
		return *this;
	};

	void MovComTemplar<T>::random() {
		int ran = rand() % 130321;
		for (int i = 0; i < 4; i++)
		{
			int unit = ran % 19;
			digits.at(i) = unit - 9;
			ran = (ran - unit) / 19;
		};
		return;
	};

	int getint() {
		int unsigned_unit = 0, result = 0;
		for (int i = 0; i < 4; i++)
		{
			int unsigned_unit = digits.at(i);
			if (unsigned_unit < 0)unsigned_unit = -unsigned_unit;
			result = result * 10 + unsigned_unit;
		};
		return result;
	};

	friend std::ostream& operator<<(std::ostream& ostr, const MovComTemplar &movcom) {
		ostr << std::showpos;
		for (int i = 0; i < 3; i++)
		{
			ostr << movcom.getdigit(i) << ",";
		};
		ostr << movcom.getdigit(3) << std::noshowpos;
		return ostr;
	};
protected:
	std::array<T, 4> digits;
};

#endif