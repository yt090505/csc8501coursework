#ifndef DIGCOM_H
#define DIGCOM_H

#include "Com.h"
#include "MovCom.h"
#include <array>

//has unique set method
class DigCom : public Com
{
public:
	DigCom();
	~DigCom();
	void setdigit(int x, int val);
	void random();
	DigCom operator= (const DigCom &rhs);
	bool isValidCN();
	friend std::ostream& operator<<(std::ostream& ostr, const DigCom &digcom);
	friend bool operator==(const DigCom &a1, const DigCom &a2) ;

	//BE CAUTIOUS USE ONLY SHOWN INSTANCE   ROOT==HN[-1]
	//D = D + M:
	//1.CN[i] = HN[i-1] + UHF
	//2.LN[i] = CN[i] + LHF
	//3.HN[i] = LN[i] + PHF
	friend DigCom operator+ (const DigCom &lhs, const MovCom &rhs);

	//FOR DECODING RESULT IS UNSIGNED
	//M = D - D:
	//1.ULPHF = LN[1]-LN[0] 
	//2.PHF = HN[0] - LN[0]  ( HN[O] = ROOT + ULPHF)
	//3.UHF = CN[0] - ROOT
	//4.LHF = LN[0] - CN[0]
	friend MovCom operator- (const DigCom &lhs, const DigCom &rhs);


protected:
};


#endif