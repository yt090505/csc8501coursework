#include "Com.h"

Com::Com() {
	digits = { 0,0,0,0 };
};

Com::~Com() {};

int Com::getdigit(int x) const { 
	return digits.at(x); 
};

void Com::setdigit(int x, int val) {
	digits.at(x) = val;
	return;
};

//randomlize a digcom with 4 digits and a movcom with four signed digits
void Com::set4digits(int x) {
	if (x < 0) x = -x;
	for (int i = 0; i < 4; i++)
	{
		int unit = x % 10;
		digits.at(3 - i) = unit;
		x = (x - unit) / 10;
	};
	return;
};