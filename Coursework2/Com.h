#ifndef COM_H
#define COM_H
#include <array>

//com is a combination of 4 digits 
//com includes 2 children :
//1 codenumbers(DigCom) namely CN LN HN
//2 hashnumbers(MovCom) namely UHF LHF PHF
//has only get method
class Com
{
public:
	Com() ;
	~Com() ;
	void set4digits(int x);
	virtual int getdigit(int x) const;
	virtual void setdigit(int x, int val);
protected:
	std::array<int, 4> digits;
};
#endif
